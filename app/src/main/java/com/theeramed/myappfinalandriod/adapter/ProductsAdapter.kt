package com.theeramed.myappfinalandriod.adapter
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable

import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.theeramed.myappfinalandriod.R
import com.theeramed.myappfinalandriod.databinding.ItemListBinding
import com.theeramed.myappfinalandriod.model.ProductsData
import com.theeramed.myappfinalandriod.view.NewActivity



class ProductsAdapter (private var c:Context,private var productLists:List<ProductsData>)
    :RecyclerView.Adapter<ProductsAdapter.ProductsViewHolder>(){

    inner class ProductsViewHolder(val v:ItemListBinding):RecyclerView.ViewHolder(v.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductsViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val v = DataBindingUtil.inflate<ItemListBinding>(inflater,R.layout.item_list,parent,false)
        return ProductsViewHolder(v)
    }

    override fun onBindViewHolder(holder: ProductsViewHolder, position: Int) {
        val productsList = productLists[position]
        holder.v.isproductList = productsList
        holder.v.productsImg.setImageResource(productsList.productsImg)
        holder.v.root.setOnClickListener{
            val product :Int = productsList.productsImg
            val productIntent = Intent(c,NewActivity::class.java)
            productIntent.putExtra("productsName",productsList.productsName)
            productIntent.putExtra("productsDetail",productsList.productsDetail)
            productIntent.putExtra("productsImg",product)
            c.startActivity(productIntent)

        }

    }
    fun filterList(filterlist: ArrayList<ProductsData>) {
        // below line is to add our filtered
        // list in our course array list.
        productLists = filterlist
        // below line is to notify our adapter
        // as change in recycler view data.
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return productLists.size
    }
        }