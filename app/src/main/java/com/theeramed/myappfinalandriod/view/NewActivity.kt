package com.theeramed.myappfinalandriod.view

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.theeramed.myappfinalandriod.MainActivity
import com.theeramed.myappfinalandriod.R
import kotlinx.android.synthetic.main.activity_new.*

class NewActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new)

        val productsData = intent
        val productsName = productsData.getStringExtra("productsName")
        val productsDetail = productsData.getStringExtra("productsDetail")
        val productsImg = productsData.getIntExtra("productsImg",0)
        product.setImageResource(productsImg)
        productName.text = productsName
        productDetail.text = productsDetail
        button.setOnClickListener{
            val intent = Intent(this,MainActivity::class.java)
            startActivity(intent)
        }










    }
}