package com.theeramed.myappfinalandriod

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import com.theeramed.myappfinalandriod.adapter.ProductsAdapter
import com.theeramed.myappfinalandriod.model.ProductsData
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.item_list.*

class MainActivity : AppCompatActivity() {
    private  lateinit var  productsAdapter: ProductsAdapter
    private  lateinit var  productsData: ArrayList<ProductsData>






    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        productsData = ArrayList<ProductsData>()
        productsAdapter = ProductsAdapter(this,productsData)
        productsRecycler.layoutManager = LinearLayoutManager(this)
        productsRecycler.adapter = productsAdapter
        listProducts()

    }


     fun listProducts(){
        productsData.add(ProductsData(R.drawable.p1,"100 PLUS","13 บาท รสชาติหวาน เปรี้ยว ซ่า คล้ายน้ำอัดลม มีกลิ่นหอม"))
        productsData.add(ProductsData(R.drawable.p2,"GATORADE","25 บาท รสชาติที่คุ้นเคยไม่หวานเกินไป กลิ่นหอมมะนาวเจือจาง เปรี้ยวปลายลิ้น พลังงาน 125 KCL"))
        productsData.add(ProductsData(R.drawable.p3,"POWDURANCE","16 บาท มีรสชาติของผลไม้รวมเบาๆ มีกลิ่มหอมผลไม้ เปรี้ยวอมหวาน"))
        productsData.add(ProductsData(R.drawable.p4,"สปอเรต","10 บาท กินง่ายมาก หวาน หอม คล้ายพวกเจลลี่"))
        productsData.add(ProductsData(R.drawable.p5,"POCARI SWEAT","25 บาท มีรสชาติเจือจาง กลิ่นหอมซิตรัส หวานอ่อนๆ ไม่เปรี้ยว"))
        productsData.add(ProductsData(R.drawable.p6,"สปอนเซอร์","10 บาท กินง่าย หวานมีกลิ่นหอม คนส่วนใหญ่ คุ้นเคยกันดี"))
        productsData.add(ProductsData(R.drawable.p7,"เอ็มพลัส (เหลือง)","10 บาท กินแล้วรู้สึกสดชื่น คล้ายไอติมฟรุตตี้ รสชาติดี กินง่ายที่สุด"))
        productsData.add(ProductsData(R.drawable.p8,"CARABAO (energy drink)","25 บาท เปรี้ยวซ่ามากเหมือนน้ำอัดลม มีกลิ่นแอปเปิ้ลชัด"))
         productsData.add(ProductsData(R.drawable.p9,"Sponsor Active Zinc","10 บาท มีส่วนผสมของซิงค์ และแอลคาเนทีน ที่มีส่วนช่วยในการเผาผลาญ อีกทั้งยังทำให้หน้าใสไร้สิวอีกด้วย ดื่มแล้วร่างกายจะรู้สึกสดชื่น และที่สำคัญก็คือ ตัวนี้ก็ยังคงเป็นสปอร์ตดริ้งค์ ที่ช่วยชดเชยน้ำและเกลือแร่ที่สูญเสียไป ระหว่างกิจกรรมต่าง ๆ"))
         productsData.add(ProductsData(R.drawable.p10,"Royal D Mix Fruit","ราคาประมาณ 69 บาท / ตกซองละ 6.90 บาท (มีทั้งหมด 10 ซอง) รสชาติอร่อย ดื่มง่าย เหมาะสำหรับผู้ที่เสียเหงื่อมาก นักกีฬา คนออกกำลังกาย หรือ คนที่ทำงานหนัก ๆ "))
         productsData.add(ProductsData(R.drawable.p11,"NEWTOWER Electrolyte Beverage","ราคาประมาณ 200 บาท / ตกซองละ 4 บาท (มีทั้งหมด 50 ซอง) รสชาติออกเปรี้ยว ๆ เค็ม ๆ ดื่มง่าย"))
    }
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // below line is to get our inflater
        val inflater = menuInflater

        // inside inflater we are inflating our menu file.
        inflater.inflate(R.menu.search_item, menu)

        // below line is to get our menu item.
        val searchItem: MenuItem = menu.findItem(R.id.searchView)

        // getting search view of our item.
        val searchView: SearchView = searchItem.getActionView() as SearchView

        // below line is to call set on query text listener method.
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener,
            android.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(p0: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(msg: String): Boolean {
                // inside on query text change method we are
                // calling a method to filter our recycler view.
                filter(msg)
                return false
            }
        })
        return true
    }

    private fun filter(text: String) {
        // creating a new array list to filter our data.
        val filteredlist: ArrayList<ProductsData> = ArrayList()

        // running a for loop to compare elements.
        for (item in productsData) {
            // checking if the entered string matched with any item of our recycler view.
            if (item.productsName.toLowerCase().contains(text.toLowerCase())) {
                // if the item is matched we are
                // adding it to our filtered list.
                filteredlist.add(item)
            }
        }
        if (filteredlist.isEmpty()) {
            // if no item is added in filtered list we are
            // displaying a toast message as no data found.
            Toast.makeText(this, " ", Toast.LENGTH_SHORT).show()
        } else {
            // at last we are passing that filtered
            // list to our adapter class.
            productsAdapter.filterList(filteredlist)
        }
    }
}



